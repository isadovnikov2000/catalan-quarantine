let sources = document.getElementById('sources');
let dailyData = document.getElementById('dailyData');
let charts = document.getElementById('charts');
const currentSource = document.getElementById('currentSource');

httpGetAsync('sources', loadSources)

function httpGetAsync(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true);
    xmlHttp.send(null);
}

function todayDailyDate(responseText) {
    const data = JSON.parse(responseText);
    const date = data.date;
    const epg = data.epg;
    const infected = data.infectedCount;
    const dead = data.deadCount;
    const rt = data.rt;
    const hospitalised = data.hospitalisedCount;
    const bls = data.blsCount;
    const v1 = data.firstStageVaccinatedCount;
    const v2 = data.secondStageVaccinatedCount;


    addTodayDailyData(date, 'Date');
    addTodayDailyData(v1, 'Vaccinated 1st dose');
    addTodayDailyData(v2, 'Vaccinated 2nd dose');
    addTodayDailyData(infected, 'Confirmed Cases');
    addTodayDailyData(dead, 'Died');
    addTodayDailyData(epg, 'EPG');
    addTodayDailyData(rt, 'RT');
    addTodayDailyData(hospitalised, 'Hospitalised');
    addTodayDailyData(bls, 'ICU');
}

function addTodayDailyData(value, header) {
    const container = document.createElement('div');
    container.className = 'col-sm';

    const headerContainer = document.createElement('div');
    headerContainer.className = 'col-sm todayHeader border-top border-right border-left border-5 d-flex justify-content-center font-weight-bold rounded-top';
    const headerNode = document.createTextNode(header);
    headerContainer.appendChild(headerNode);

    const valueContainer = document.createElement('div');
    valueContainer.className = 'col-sm border border-5 d-flex justify-content-center rounded-bottom';
    const valueNode = document.createTextNode(value);
    valueContainer.appendChild(valueNode);

    container.appendChild(headerContainer);
    container.appendChild(valueContainer);
    dailyData.appendChild(container);
}

function drawCharts(responseText) {
    const data = JSON.parse(responseText);
    let length = data.stats.length;
    let labels = [];
    let epg = [];
    let infected = [];
    let dead = [];
    let rt = [];
    let hospitalised = [];
    let bls = [];
    let v1 = [];
    let v2 = [];
    for (let i = 0; i < length; i++) {
        let row = data.stats[i];
        labels.push(row.date);
        epg.push(row.epg);
        infected.push(row.infectedCount);
        dead.push(row.deadCount);
        rt.push(row.rt);
        hospitalised.push(row.hospitalisedCount);
        bls.push(row.blsCount);
        v1.push(row.firstStageVaccinatedCount);
        v2.push(row.secondStageVaccinatedCount);
    }

    drawChart('infected', labels, [{
        label: 'Confirmed Cases',
        data: infected,
    }]);
    drawChart('dead', labels, [{
        label: 'Died',
        data: dead,
    }]);
    drawChart('rt', labels, [{
        label: 'RT',
        data: rt,
    }]);
    drawChart('epg', labels, [{
        label: 'EPG',
        data: epg,
    }]);
    drawChart('hospital', labels, [{
        label: 'Hospitalised',
        data: hospitalised,
        borderColor: '#ffa600',
    }, {
        label: 'Intensive Carry(ICU)',
        data: bls,
        borderColor: '#ff6361',
    }]);
    drawChart('Vaccinated', labels, [{
        label: 'Vaccinated 1st dose',
        data: v1,
        borderColor: '#236be5',
    }, {
        label: 'Vaccinated 2nd dose',
        data: v2,
        borderColor: '#58de28',
    }]);
}

function drawChart(chartId, labels, datasets) {

    const container = document.createElement('div');
    let className = 'col-sm';
    if ($(window).width() >= 980) {
        className = 'col-sm-6'
    }
    container.className = className;
    const chart = document.createElement('canvas');
    chart.id = chartId;

    container.appendChild(chart);
    charts.appendChild(container);

    new Chart(chart, {
        type: 'line',
        data: {
            labels: labels,
            datasets: datasets
        },
    });
}

function loadSources(responseText) {
    const data = JSON.parse(responseText);
    const sourcesData = data.sources;
    const length = data.sources.length;

    reloadData(sourcesData[0])

    for (let i = 0; i < length; i++) {
        const source = sourcesData[i];
        const sourceName = source.name;
        const valueNode = document.createTextNode(sourceName);

        const sourceLink = document.createElement('a');
        sourceLink.className = 'dropdown-item';
        sourceLink.href = '#';
        sourceLink.onclick = () => {
            reloadData(source)
        }

        sourceLink.appendChild(valueNode);
        sources.appendChild(sourceLink);
    }
}

function reloadData(source) {
    dailyData.innerHTML = '';
    charts.innerHTML = '';
    currentSource.innerHTML = '';

    const sourceName = source.name;
    const valueNode = document.createTextNode(sourceName);
    const sourceLink = document.createElement('a');
    sourceLink.href = 'https://'+source.host;
    sourceLink.appendChild(valueNode);
    currentSource.appendChild(sourceLink);

    httpGetAsync('catalonia-covid-stats/' + sourceName + '/todaydata', todayDailyDate)
    httpGetAsync('catalonia-covid-stats?from=1994-06-03&to=2094-06-03&source=' + sourceName, drawCharts)
}
