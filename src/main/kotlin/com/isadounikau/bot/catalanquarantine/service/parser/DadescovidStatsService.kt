package com.isadounikau.bot.catalanquarantine.service.parser

import com.isadounikau.bot.catalanquarantine.common.Source
import com.isadounikau.bot.catalanquarantine.service.modal.DailyStats
import com.isadounikau.bot.catalanquarantine.service.modal.VaccinationStats
import org.jsoup.Jsoup
import org.jsoup.select.Elements
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class DadescovidStatsService : StatsParseService {

    override fun getDailyStats(): DailyStats {
        val doc = Jsoup.connect(URL).get()

        val covidDadas = doc.select("#content_block")

        val dateRow = covidDadas[0].select(".row")[1].select("h4")
        val date = try {
            dateRow.text().substring(11).let {
                LocalDate.parse(it, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            LocalDate.now()
        }

        val covidDada = covidDadas[0].select(".row")[2].select("td")
        val epg = try {
            getInt(5, covidDada)
        } catch (ex: Exception) {
            ex.printStackTrace()
            0
        }
        val infected = try {
            getInt(4, covidDada)
        } catch (ex: Exception) {
            ex.printStackTrace()
            0
        }
        val dead = try {
            getInt(6, covidDada)
        } catch (ex: Exception) {
            ex.printStackTrace()
            0
        }
        val rt = try {
            val rt = covidDadas.select(".row")[0].select("td")[5].text()
            rt.replace("(", "").replace(")", "").replace(",", ".").toDouble()
        } catch (ex: Exception) {
            ex.printStackTrace()
            0.0
        }
        val hospitalised = try {
            getInt(2, covidDada)
        } catch (ex: Exception) {
            ex.printStackTrace()
            0
        }
        val bls = try {
            getInt(3, covidDada)
        } catch (ex: Exception) {
            ex.printStackTrace()
            0
        }
        val firstStageVaccinatedCount = try {
            getInt(0, covidDada)
        } catch (ex: Exception) {
            ex.printStackTrace()
            0
        }

        val secondStageVaccinatedCount = try {
            getInt(1, covidDada)
        } catch (ex: Exception) {
            ex.printStackTrace()
            0
        }

        return DailyStats(
            source = Source.DADESCOVID,
            date = date,
            epg = epg,
            infectedCount = infected,
            deadCount = dead,
            rt = rt,
            hospitalisedCount = hospitalised,
            blsCount = bls,
            vaccinatedStats = VaccinationStats(
                firstStageVaccinatedCount = firstStageVaccinatedCount,
                secondStageVaccinatedCount = secondStageVaccinatedCount,
            ),
        )
    }

    private fun getInt(index: Int, covidDada: Elements) = covidDada[index].text().replace("(", "").replace(")", "").replace(".", "").toInt()

    companion object {
        const val URL = "http://dadescovid.cat/"
    }
}
