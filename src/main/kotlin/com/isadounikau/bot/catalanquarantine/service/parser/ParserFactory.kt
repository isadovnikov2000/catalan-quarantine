package com.isadounikau.bot.catalanquarantine.service.parser

import com.isadounikau.bot.catalanquarantine.common.Source
import javax.inject.Singleton

@Singleton
class ParserFactory {

    private val parserMap = mapOf<Source, StatsParseService>()

    fun getStatsParseService(source: Source): StatsParseService = when(source) {
        Source.DADESCOVID -> parserMap.getOrDefault(source, DadescovidStatsService())
        Source.NACIODIGITAL -> parserMap.getOrDefault(source, NaciodigitalStatsService())
    }

}
