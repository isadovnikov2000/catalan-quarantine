package com.isadounikau.bot.catalanquarantine.service

import com.isadounikau.bot.catalanquarantine.common.Source
import com.isadounikau.bot.catalanquarantine.service.modal.DailyStats
import com.isadounikau.bot.catalanquarantine.service.modal.VaccinationStats
import com.isadounikau.bot.catalanquarantine.service.parser.ParserFactory
import com.isadounikau.bot.catalanquarantine.storage.modal.DailyStatsEntity
import com.isadounikau.bot.catalanquarantine.storage.repository.DailyStatsEntityRepository
import java.time.LocalDate
import javax.inject.Singleton
import javax.transaction.Transactional

interface DailyStatsService {
    fun parseDailyStats(source: Source): DailyStats
    fun getDailyStats(date: LocalDate, source: Source): DailyStats?
    fun getDailyStats(from: LocalDate, to: LocalDate, source: Source): List<DailyStats>
    fun saveDailyStats(dailyStats: DailyStats)
}

@Singleton
class DefaultDailyStatsService(
    private val sourceFactory: ParserFactory,
    private val repository: DailyStatsEntityRepository,
) : DailyStatsService {

    override fun parseDailyStats(source: Source): DailyStats {
        return sourceFactory.getStatsParseService(source).getDailyStats().also {
            try {
                this.saveDailyStats(it)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    override fun getDailyStats(date: LocalDate, source: Source): DailyStats? {
        return repository.findByDateAndSource(date, source)?.toDailyStats()
    }

    override fun getDailyStats(from: LocalDate, to: LocalDate, source: Source): List<DailyStats> {
        return repository.listByDateAndSource(from, to, source).map { it.toDailyStats() }
    }

    @Transactional
    override fun saveDailyStats(dailyStats: DailyStats) {
        val entity = repository.findByDateAndSource(dailyStats.date, dailyStats.source)
        if (entity == null) {
            repository.persist(dailyStats.toDailyStatsEntity())
        } else {
            //TODO write update
        }
    }

}

fun DailyStats.toDailyStatsEntity(): DailyStatsEntity = DailyStatsEntity().also {
    it.source = this.source.name
    it.statisticsDate = this.date
    it.epg = this.epg
    it.infectedCount = this.infectedCount
    it.deadCount = this.deadCount
    it.rt = this.rt
    it.hospitalisedCount = this.hospitalisedCount
    it.blsCount = this.blsCount
    it.firstStageVaccinatedCount = this.vaccinatedStats?.firstStageVaccinatedCount
    it.secondStageVaccinatedCount = this.vaccinatedStats?.secondStageVaccinatedCount
}

fun DailyStatsEntity.toDailyStats(): DailyStats = DailyStats(
    Source.valueOf(source),
    statisticsDate,
    epg,
    infectedCount,
    deadCount,
    rt,
    hospitalisedCount,
    blsCount,
    VaccinationStats(
        firstStageVaccinatedCount,
        secondStageVaccinatedCount,
    ),
)

