package com.isadounikau.bot.catalanquarantine.service.modal

import com.isadounikau.bot.catalanquarantine.common.Source
import java.time.LocalDate

data class DailyStats(
    val source: Source,
    val date: LocalDate,
    val epg: Int?,
    val infectedCount: Int?,
    val deadCount: Int?,
    val rt: Double?,
    val hospitalisedCount: Int?,
    val blsCount: Int?,
    val vaccinatedStats: VaccinationStats?,
)

data class VaccinationStats(
    val firstStageVaccinatedCount: Int?,
    val secondStageVaccinatedCount: Int?,
)
