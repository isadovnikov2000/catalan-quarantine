package com.isadounikau.bot.catalanquarantine.storage.modal

import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "covid_daily_statistics")
open class DailyStatsEntity {
    @Id
    @GeneratedValue(generator = "covid_daily_statistics_id")
    open var id: Long? = null
    open lateinit var source: String
    @Column(name = "statistics_date")
    open lateinit var statisticsDate: LocalDate
    open var epg: Int? = null
    @Column(name = "infected_count")
    open var infectedCount: Int? = null
    @Column(name = "dead_count")
    open var deadCount: Int? = null
    @Column(name = "rt")
    open var rt: Double? = null
    @Column(name = "hospitalised_count")
    open var hospitalisedCount: Int? = null
    @Column(name = "bls_count")
    open var blsCount: Int? = null
    @Column(name = "first_stage_vaccinated_count")
    open var firstStageVaccinatedCount: Int? = null
    @Column(name = "second_stage_vaccinated_count")
    open var secondStageVaccinatedCount: Int? = null
}
