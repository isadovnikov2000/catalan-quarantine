package com.isadounikau.bot.catalanquarantine.storage.repository

import com.isadounikau.bot.catalanquarantine.common.Source
import com.isadounikau.bot.catalanquarantine.storage.modal.DailyStatsEntity
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import io.quarkus.panache.common.Parameters
import java.time.LocalDate
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
open class DailyStatsEntityRepository : PanacheRepository<DailyStatsEntity> {
    fun findByDateAndSource(date: LocalDate, source: Source): DailyStatsEntity? {
        val params = Parameters()
            .and("date", date)
            .and("source", source.name)
        return find("statisticsDate = :date and source = :source", params).firstResult()
    }

    fun listByDateAndSource(from: LocalDate, to: LocalDate, source: Source): List<DailyStatsEntity> {
        val params = Parameters()
            .and("from", from)
            .and("to", to)
            .and("source", source.name)
        return find("statisticsDate >= :from AND statisticsDate <= :to AND source = :source", params).list()
    }

}
