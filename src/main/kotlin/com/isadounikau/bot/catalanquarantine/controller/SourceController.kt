package com.isadounikau.bot.catalanquarantine.controller

import com.isadounikau.bot.catalanquarantine.common.Source
import org.eclipse.microprofile.openapi.annotations.tags.Tag
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/sources")
@Tag(name = "Data Source Controller", description = "API related to data sources")
class SourceController {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getAllSources(): SourceResponseMessages = Source.values().map {
        SourceResponseMessage(it.name.toLowerCase(), it.host)
    }.let {
        SourceResponseMessages(it)
    }
}

data class SourceResponseMessages(
    val sources: List<SourceResponseMessage>,
)

data class SourceResponseMessage(
    val name: String,
    val host: String,
)
