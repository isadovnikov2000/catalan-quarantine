package com.isadounikau.bot.catalanquarantine.controller

import com.isadounikau.bot.catalanquarantine.common.Source
import com.isadounikau.bot.catalanquarantine.service.DailyStatsService
import com.isadounikau.bot.catalanquarantine.service.modal.DailyStats
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter
import org.eclipse.microprofile.openapi.annotations.tags.Tag
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType

@ApplicationScoped
@Path("/catalonia-covid-stats")
@Tag(name = "Covid Data For Catalonia Resource", description = "Basic API for data")
class CatalanCovidStatsController(
    private val dailyStatsService: DailyStatsService,
) {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getStatsTelegramMessage(
        @Parameter(example = "dadescovid") @QueryParam("source") sourceString: String,
        @Parameter(example = "1994-06-03") @QueryParam("from") fromDateString: String?,
        @Parameter(example = "2094-06-03") @QueryParam("to") toDateString: String?,
    ): DailyStatsResponseMessages {
        val source = Source.valueOf(sourceString.toUpperCase())
        val from = fromDateString?.let { LocalDate.parse(fromDateString) } ?: LocalDate.now()
        val to = fromDateString?.let { LocalDate.parse(toDateString) } ?: LocalDate.now()

        return dailyStatsService.getDailyStats(from, to, source)
            .map { it.toDailyStatsResponseMessage() }
            .let { DailyStatsResponseMessages(it) }
    }

    @GET
    @Path("/{source}/todaydata")
    @Produces(MediaType.APPLICATION_JSON)
    fun getTodayStats(
        @Parameter(example = "dadescovid") @PathParam("source") source: String,
    ): DailyStatsResponseMessage = with(source) { Source.valueOf(this.toUpperCase()) }
        .let {
            val date = when (it) {
                Source.DADESCOVID -> LocalDate.now().minusDays(1) //workaround because fresh stats always for day before
                else -> LocalDate.now()
            }
            dailyStatsService.getDailyStats(date, it) ?: dailyStatsService.parseDailyStats(it)
        }
        .run { this.toDailyStatsResponseMessage() }
}

data class DailyStatsResponseMessages(
    val stats: List<DailyStatsResponseMessage>,
)

data class DailyStatsResponseMessage(
    val date: String,
    val epg: Int?,
    val infectedCount: Int?,
    val deadCount: Int?,
    val rt: Double?,
    val hospitalisedCount: Int?,
    val blsCount: Int?,
    val firstStageVaccinatedCount: Int?,
    val secondStageVaccinatedCount: Int?,
)

fun DailyStats.toDailyStatsResponseMessage(): DailyStatsResponseMessage = DailyStatsResponseMessage(
    date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
    epg, infectedCount, deadCount, rt, hospitalisedCount, blsCount,
    vaccinatedStats?.firstStageVaccinatedCount,
    vaccinatedStats?.secondStageVaccinatedCount
)
