./mvnw clean package -Dquarkus.package.type=uber-jar
docker build -f src/main/docker/Dockerfile.heroku -t quarkus/hello-quarkus .
docker tag quarkus/hello-quarkus registry.heroku.com/catalan-quarantine/web
docker push registry.heroku.com/catalan-quarantine/web
heroku container:rm web -a catalan-quarantine
heroku container:release web -a catalan-quarantine
